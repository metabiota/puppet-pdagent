# == Class pdagent::install
#
# This class is called from pdagent for install.
#
class pdagent::install {

  package { $::pdagent::package_name:
    ensure => present,
  }

  package { $::pdagent::integrations_package_name:
    ensure => present,
  }

  file { '/usr/lib/zabbix/alertscripts/pd-zabbix':
    ensure => 'link',
    target =>  '/usr/share/pdagent-integrations/bin/pd-zabbix'
  }
}
